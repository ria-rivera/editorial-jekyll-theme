---
layout: default
---

<!-- Section -->

<!-- Section -->
<section>
	<header class="major">
		<h2>Posts</h2>
	</header>
	<div class="posts">
		<article>
			<a href="#" class="image"><img src="assets/images/pic05.jpg" alt="" /></a>
			<h3>Create a One-Page Site on Carrd.co</h3>
			<p>You can create a snazzy one-page site in just minutes using one of several Carrd.co templates —for free.</p>
			<ul class="actions">
				<li><a href="#" class="button">More</a></li>
			</ul>
		</article>
		<article>
			<a href="#" class="image"><img src="assets/images/pic05.jpg" alt="Hand-drawn GitLab logo" /></a>
			<h3>GitLab for Artists: Using GitLab Repositories for Online Storage</h3>
			<p>We can't talk about GitHub without mentioning GitLab. Spoiler alert: it's equally awesome.</p>
			<ul class="actions">
				<li><a href="#" class="button">More</a></li>
			</ul>
		</article>
		<article>
			<a href="#" class="image"><img src="assets/images/pic05.jpg" alt="Hand-drawn GitHub logo" /></a>
			<h3>GitHub for Artists: Using GitHub Repositories for Online Storage</h3>
			<p>GitHub isn't just for developers. You can use GitHub repositories to store your images, documents, and other assets.</p>
			<ul class="actions">
				<li><a href="#" class="button">More</a></li>
			</ul>
		</article>
		<!---<article>
			<a href="#" class="image"><img src="assets/images/pic04.jpg" alt="" /></a>
			<h3>Sed etiam facilis</h3>
			<p>Aenean ornare velit lacus, ac varius enim lorem ullamcorper dolore. Proin aliquam facilisis ante interdum. Sed nulla amet lorem feugiat tempus aliquam.</p>
			<ul class="actions">
				<li><a href="#" class="button">More</a></li>
			</ul>
		</article>
		<article>
			<a href="#" class="image"><img src="assets/images/pic05.jpg" alt="" /></a>
			<h3>Feugiat lorem aenean</h3>
			<p>Aenean ornare velit lacus, ac varius enim lorem ullamcorper dolore. Proin aliquam facilisis ante interdum. Sed nulla amet lorem feugiat tempus aliquam.</p>
			<ul class="actions">
				<li><a href="#" class="button">More</a></li>
			</ul>
		</article>
		<article>
			<a href="#" class="image"><img src="assets/images/pic06.jpg" alt="" /></a>
			<h3>Amet varius aliquam</h3>
			<p>Aenean ornare velit lacus, ac varius enim lorem ullamcorper dolore. Proin aliquam facilisis ante interdum. Sed nulla amet lorem feugiat tempus aliquam.</p>
			<ul class="actions">
				<li><a href="#" class="button">More</a></li>
			</ul>
		</article>--->
	</div>
</section>
